from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class User(AbstractUser):
    name = models.CharField(_('Full name'), max_length=255, blank=True)
    password = models.CharField(max_length=250)
    email = models.EmailField(_('email address'), unique=True)
    is_active = models.BooleanField(_('Active'), default=True)
    date_of_birthday = models.DateField(_('date of birthday'), null=True, blank=True,)
    avatar = models.ImageField(null=True)
    date_joined = models.DateTimeField(_('Date joined'), auto_now_add=True)
    created_time = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_time = models.DateTimeField(auto_now_add=False, auto_now=True)
    token = models.CharField(max_length=255, null=True, unique=True)
    # Passport data
    series = models.CharField(max_length=50, null=True)
    number = models.CharField(max_length=50, null=True)
    issued_by = models.CharField(max_length=255, null=True)
    when_issued = models.DateField(null=True)
    passport_img = models.ImageField(null=True)
    groups = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True)


    # created_by = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL)
    # updated_by = models.ForeignKey('self', null=True, on_delete=models.SET_NULL)
    # created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='created_by_user', null=True, on_delete=models.SET_NULL)
    # updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='updated_by_user', null=True, on_delete=models.SET_NULL)
    # region = models.ForeignKey(Regions, on_delete=models.SET_NULL, null=True)
    # district = models.ForeignKey(Districts, on_delete=models.SET_NULL, null=True)
    
    def __str__(self):
        return self.username
