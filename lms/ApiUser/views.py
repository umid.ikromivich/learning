from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.views import APIView
from Users.models import User
# from rest_framework.views import APIView
from .serializers import  RoleSerializer, UserLoginSerializer, UserProfileSerializer, UserRegisterSerializer
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
import jwt
from django.contrib.auth.models import update_last_login
from rest_framework.generics import GenericAPIView, ListAPIView, RetrieveUpdateAPIView

# Create your views here.

class RegisterView(GenericAPIView):
    serializer_class = UserRegisterSerializer
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()      
        
        return Response({
        "user": UserRegisterSerializer(user, context=self.get_serializer_context()).data,
        })


   

class LoginView(GenericAPIView):
    serializer_class = UserLoginSerializer
    def post(self, request):
        username = request.data['username']
        password = request.data['password']

        user = User.objects.filter(username=username).first()

        if user is None:
            raise AuthenticationFailed('User not found!')
        
        if not user.check_password(password):
            raise AuthenticationFailed('Incorrect password!')
        
        response = Response()
        update_last_login(None, user)
        if user.groups:
            group = Group.objects.get(id=user.groups.id)
            group = {
                'id': group.id,
                'name': group.name
            }
        else:
            group = None

        response.data = {
            'username' : user.username,
            'token': user.token,
            'groups': group
        }

        return response



class UserProfileView(RetrieveUpdateAPIView):
    serializer_class = UserProfileSerializer
    def get(self, request):
        token = request.headers['token']
        if not token:
            raise AuthenticationFailed('Unauthenticated')
        try:
            payload = jwt.decode(token, 'hope-inspire', algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated')
        user = User.objects.filter(id=payload['id']).first()
        if user is None:
            raise AuthenticationFailed('User not found!')
        serializer = UserProfileSerializer(user)
        if serializer.data['last_login'] is None:
            return Response(status = status.HTTP_403_FORBIDDEN)
        else:
            return Response(serializer.data)


    def put(self, request):
        token = request.headers['token']
        if not token:
            raise AuthenticationFailed('Unauthenticated')
        try:
            payload = jwt.decode(token, 'hope-inspire', algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated')
        user = User.objects.filter(id=payload['id']).first()
        if user is None:
            raise AuthenticationFailed('User not found!')
        serializer = self.serializer_class(user, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return Response(serializer.data, status.HTTP_202_ACCEPTED)


class RoleListView(ListAPIView):
    serializer_class = RoleSerializer
    def get_queryset(self):
        return Group.objects.all()

# class LogoutView(APIView):
#     def post(self, request):
#         response = Response()
#         response.delete_cookie('token')
#         response.data = {
#             'message': 'success'
#         }
#         return response