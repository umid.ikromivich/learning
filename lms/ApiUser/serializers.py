from django.contrib.auth.models import Group
from django.db.models.base import Model
from rest_framework import fields, serializers
from Users.models import User
import jwt, datetime





class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name', 'username', 'password', 'email', 'groups', 'token']
        extra_kwargs = {
            'password': {'write_only': True},
            'token': {'read_only': True},
        }

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        payload = {
            'id': instance.id,
            # 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30),
            'iat': datetime.datetime.utcnow()
        }

        token = jwt.encode(payload, 'hope-inspire', algorithm='HS256').decode('utf-8')
        instance.token = token
        instance.save()
        return instance



class UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'last_login']
        extra_kwargs = {
            'password': {'write_only': True},
            'last_login': {'read_only': True},
        }



class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name']


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['last_login', 'username', 'name', 'email', 'is_active', 'date_of_birthday', 'avatar', 'date_joined', 'created_time', 'updated_time', 'series', 'number', 'issued_by', 'when_issued', 'passport_img', 'groups']
        
        extra_kwargs = {
            'last_login': {'read_only': True},
            'date_joined': {'read_only': True},
            'created_time': {'read_only': True},
            'updated_time': {'read_only': True},
            'groups': {'read_only': True},
        }